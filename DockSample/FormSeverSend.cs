﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Lon.IO.Ports;

namespace DockSample
{
    public partial class FormSeverSend : Form, IMessageOuter
    {
        public FormSeverSend()
        {
            InitializeComponent();
        }

        #region IMessageOuter 成员

        public void Out(string Mess)
        {
            this.Invoke(new MethodInvoker(delegate {

                this.txtMessageOut.AppendText(Mess + "\r\n");

            }));
        }

        #endregion
    }
}
