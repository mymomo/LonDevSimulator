﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using WeifenLuo.WinFormsUI.Docking;
using System.IO;

namespace DockSample
{
    public partial class FormDigitBat : DockContent
    {
        public FormDigitBat()
        {
            InitializeComponent(); 
        }

        public void OpenFile()
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if(ofd.ShowDialog()!= DialogResult.OK)
            {
                return;
            }
           String[] lines= File.ReadAllLines(ofd.FileName);
        }
        private ToolStripMenuItem menu = new ToolStripMenuItem("开关量批量生成");
        public ToolStripMenuItem Menu
        {
            get;
            set;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            List<String> suffixNames = GetStringList(txtSuffix.Lines);
            List<String> devNames=GetStringList(txtDeviceName.Lines);
            List<String> digitNames = new List<string>();
            for (int i = 0; i < devNames.Count; i++)
            {
                for (int j = 0; j < suffixNames.Count; j++)
                {
                    if (suffixNames[j].ToUpper() == "DUMMY")
                    {
                        digitNames.Add("DUMMY");
                        continue;
                    }
                    digitNames.Add(String.Format("{0}{1}{2}", devNames[i], txtLinkChar.Text, suffixNames[j]));
                }
            }
            txtDigit.Lines = digitNames.ToArray();
            Clipboard.SetDataObject(this.txtDigit.Text, true); 
        }

        private List<String> GetStringList(String[] lines)
        {
            List<String> res = new List<string>();
            for (int i = 0; i < lines.Length; i++)
            {
                if (String.IsNullOrEmpty(lines[i]))
                {
                    continue;
                }
                res.Add(lines[i].Trim());
            }
            return res;
        }

       
    }
}
