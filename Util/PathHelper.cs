﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Lon.Util
{
   public  class PathHelper
    {
        public static string GetApplicationPath()
        {
            string baseDirectory =  AppDomain.CurrentDomain.BaseDirectory;
            return baseDirectory;
        }
        public static string GetApplicationName()
        {
            string str = Application.ExecutablePath;
            char[] chrArray = new char[1];
            chrArray[0] = '\\';
            string[] strs = str.Split(chrArray);
            string str1 = strs[(int)strs.Length - 1].Replace(".exe", "");
            return str1;
        }
    }
}
