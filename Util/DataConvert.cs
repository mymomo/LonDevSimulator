using System;

//公共数据处理单元
namespace Lon.Util
{
	/// <summary>
	/// 数据类型转换类
	/// </summary>
	public class DataConvert
	{
		/// <summary>
		/// 将32位整数转换到字节数组
		/// </summary>
		/// <param name="dwNum">32位整数数据</param>
		/// <param name="mData">要转换到的字节数组</param>
		/// <param name="index">转换到字节数组的位置</param>
		public static void UInt32toBytes(UInt32 dwNum, byte[] mData, int index)
		{
			mData[index] = (byte)dwNum;
			mData[index + 1] = (byte)(dwNum >> 8);
			mData[index + 2] = (byte)(dwNum >> 16);
			mData[index + 3] = (byte)(dwNum >> 24);
		}


		/// <summary>
		/// 将字节数组的数据转换到32位整数
		/// </summary>
		/// <param name="mData">字节数组</param>
		/// <param name="index">开始转换的字节数组位置</param>
		/// <returns>32位整数</returns>
		public static UInt32 BytestoUint32(byte[] mData, int index)
		{
			uint num = (UInt32)( mData[index] | (mData[index+1]<<8) | (mData[index+2]<<16) | (mData[index+3]<<24));
			return num;
		}


		/// <summary>
		/// 将16位整数转换到字节数组
		/// </summary>
		/// <param name="wNum">16位整数数据</param>
		/// <param name="mData">要转换到的字节数组</param>
		/// <param name="index">转换到字节数组的位置</param>
		static public void UInt16toBytes(UInt16 wNum, byte[] mData, int index)
		{
			mData[index] = (byte)wNum;
			mData[index + 1] = (byte)(wNum >> 8);
		}


		/// <summary>
		/// 将字节数组的数据转换到16位整数
		/// </summary>
		/// <param name="mData">字节数组</param>
		/// <param name="index">开始转换的字节数组位置</param>
		/// <returns>16位整数</returns>
		public static UInt16 BytestoUInt16(byte[] mData, int index)
		{
			UInt16 num = (UInt16)( mData[index] |(mData[index+1]<<8));
			return num;
		}


		/// <summary>
		/// 将点分形式的IP地址转换成4字节地址
		/// </summary>
		/// <param name="ip">IP地址</param>
		/// <returns>4字节形式的IP地址</returns>
		public static UInt32 IPParse(string ip)
		{
			UInt32 dwNum = 0;
			try
			{
				byte bNum1 = 0, bNum2, bNum3, bNum4;
				string[] strIP = ip.Split('.');
				bNum1 = byte.Parse(strIP[0]);
				bNum2 = byte.Parse(strIP[1]);
				bNum3 = byte.Parse(strIP[2]);
				bNum4 = byte.Parse(strIP[3]);

				dwNum = bNum1;
				dwNum = (UInt32)((dwNum << 8) | bNum2);
				dwNum = (UInt32)((dwNum << 8) | bNum3);
				dwNum = (UInt32)((dwNum << 8) | bNum4);

			}
			catch
			{
			}
			return dwNum;
		}



		//比较两字节数组是否相等
		public static bool ArrayIsEqual(byte[] ary1, byte[] ary2, int start, int len)
		{
			if((ary1 == null) || (ary2 == null)) {	return false;	}
			
			if (ary1.Length != ary2.Length)	{	return false;	}

			for(int i=start; i<len; i++)
			{
				if(ary1[i] != ary2[i])
				{
					return false;
				}
			}

			return true;
		}



		/// <summary>
		/// 返回一个byte[]从第index个元素后长度为length的byte[]
		/// </summary>
		/// <param name="by"></param>
		/// <param name="index"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		public static byte[] SubByteArray(byte[] by,int index,int length)
		{
			byte[] byr = new byte[length];
			for (int i = index; i < index + length; i++)
			{
				byr[i-index] = by[i];
			}
			return byr;
		}


		/// <summary>
		/// 返回一个byte[]从第index个元素后的byte[]
		/// </summary>
		/// <param name="by"></param>
		/// <param name="index"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		public static byte[] SubByteArray(byte[] by, int index)
		{
			return SubByteArray(by, index, by.Length - index);
           
		}


		/// <summary>
		/// 返回两个byte数组连接后的byte数组。.
		/// </summary>
		public static byte[] ByteAdd(byte[] by1, byte[] by2)
		{
			int l1 = by1.Length;
			int l2 = by2.Length;
			byte[] by = new byte[l1 + l2];
			for (int i = 0; i < l1; i++)
			{
				by[i] = by1[i];
			}
			for (int i = 0; i < l2; i++)
			{
				by[i + l1] = by2[i];
			}
			return by;
		}


		/// <summary>
		/// "aabbcc"变为"ccbbaa"
		/// </summary>
		/// <param name="str">长度必需是偶数</param>
		/// <returns></returns>
		public static string StrTOStrV(string str)
		{
			string retn="";
			int length=str.Length;
			for(int i=0;i<=length-2;i+=2)
			{
				retn=str.Substring(i,2)+retn;
			}
			return retn;
		}
			

        /// <summary>
        /// 交换字的高低字节
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static UInt16 InvWord(UInt16 value)
        {
            UInt16 num = (UInt16)( ((value << 8) & 0xFF00) | ((value >> 8) & 0x00FF) );
            return num;
        }

        /// <summary>
        /// 交换双字的高低字节
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static UInt32 InvDWord(UInt32 value)
        {
            UInt32 num = (UInt32)( ((value << 24) & 0xFF000000) | ((value << 8) & 0x00FF0000) | ((value >> 8) & 0x0000FF00) | ((value >> 24) & 0x000000FF) );
            return num;
        }

		/// <summary>
		/// 把一个字节数组转化成十六进制的字符串形式，以空格隔开。
		/// </summary>
		public static string ByteToHexStr(byte[] da)
		{
			string s="";
			for(int i=0;i<da.Length;i++)
			{
				s+=Convert.ToString(da[i],16).PadLeft(2,'0')+" ";
			}
			return s;
		}


		/// <summary>
		/// 把一个字节数组转化成十六进制的字符串形式
		/// </summary>
		public static string ByteToHexStr2(byte[] da)
		{
			string s="";
			for(int i=0;i<da.Length;i++)
			{
				s+=Convert.ToString(da[i],16).PadLeft(2,'0');
			}
			return s;
		}


		/// <summary>
		/// 把诸如：23 fe e3 的字符串转化成byte[]
		/// </summary>
		/// <param name="da"></param>
		/// <returns></returns>
		public static byte[] StrToHexByte(string da)
		{
			string sends=da;
			sends=sends.Replace(" ","");//去掉空格
			sends=sends.Replace("\n","");
			sends=sends.Replace("\r","");
			int length=sends.Length/2;
			byte[] sendb=new byte[length];
			
			for(int i=0;i<length;i++)
			{					
				sendb[i]=Convert.ToByte(sends.Substring(i*2,2),16);				
			}
						
			return(sendb);	
		}


		/// <summary>
		/// 判断是否数字
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public static bool IsDigit(string str)//判断是否数字
		{
			for(int i=0;i<str.Length;i++)
			{
				if(!(str[i]>='0'&&str[i]<='9'))
					return false;
			}
			return true;
		}


		/// <summary>
		/// 把8位字符byt的从第begin字符到end字符转化为二进制字符串
		/// </summary>
		/// <param name="begin"></param>
		/// <param name="end"></param>
		/// <param name="byt"></param>
		/// <returns></returns>
		public static string BitDivision(int begin,int end,byte byt)
		{
			string str1="1";
			str1=str1.PadRight(end-begin+1,'1');
			str1=str1.PadLeft(end,'0');
			str1=str1.PadRight(8,'0');
			int a=Convert.ToInt32(str1,2);
			int b=byt;
			int c=a&b;
			c=c>>(8-end);
			return Convert.ToString(c,2).PadLeft(end-begin+1,'0');
		}


		/// <summary>
		/// 把二进制字符串转化为十六进制字符串.
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public static string ToSix(string str)
		{
			long l=Convert.ToInt64(str,2);
			return "0x"+Convert.ToString(l,16).PadLeft(2,'0');
		}
		/// <summary>
		/// 把二进制字符串转化为十进制字符串
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		public static string ToTen(string str)
		{
			long l=Convert.ToInt64(str,2);
			return Convert.ToString(l,10);
		}


		/// <summary>
		/// "ef"-----"11101111"
		/// </summary>
		/// <param name="hex"></param>
		/// <returns></returns>
		public static string HexStrToBinStr(string hex)
		{
			int l=hex.Length*4;
			return Convert.ToString(Convert.ToInt32(hex,16),2).PadLeft(l,'0');

		}


		/// <summary>
		/// "1110010"--------"1,1,1,0,0,1,0"
		/// </summary>
		/// <param name="bin"></param>
		/// <returns></returns>
		public static string BinDotBin(string bin)
		{
			string rtn="";
			for(int i=0;i<bin.Length;i++)
			{
				rtn+=bin.Substring(i,1)+",";

			}
			return rtn.TrimEnd(new char[]{','});
		}
		
		
	}
}
