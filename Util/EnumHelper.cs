﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Reflection;

namespace Lon.Util
{

    public class EnumHelper
    {
        private static Dictionary<String,String> enumStringHash = new Dictionary<String,String>(500);
        private static Hashtable enumValueHash = new Hashtable();
        private static Dictionary<string, List<string>> typeHash = new Dictionary<string, List<string>>();

        public static int GetEnumValue(System.Type enumType, string enumDisStr)
        {
            string key = enumType.ToString() + enumDisStr;
            if (enumValueHash.Contains(key))
            {
                return (int)enumValueHash[key];
            }
            int ret = -1;
            MemberInfo[] mis = enumType.GetMembers();
            foreach (MemberInfo mi in mis)
            {
                Attribute[] attrs = Attribute.GetCustomAttributes(mi);
                foreach (Attribute at in attrs)
                {
                    if (at is EnumStringAttribute)
                    {
                        EnumStringAttribute esr = (EnumStringAttribute)at;
                        if (esr.GetEnumString() == enumDisStr)
                        {
                            ret = (int)Enum.Parse(enumType, mi.Name);
                            break;
                        }
                    }
                }
                if (ret != -1)
                {
                    break;
                }
            }
            enumValueHash[key] = ret;
            return ret;
        }

        public static string GetEnumDisString(System.Type enumType, int enumVal)
        {
            string key = enumType.ToString() + enumVal.ToString();
            if (enumStringHash.ContainsKey(key))
            {
                return enumStringHash[key];
            }
            string ret = "";
            MemberInfo[] mis = enumType.GetMembers();
            foreach (MemberInfo mi in mis)
            {
                Attribute[] attrs = Attribute.GetCustomAttributes(mi);
                foreach (Attribute at in attrs)
                {
                    if (at is EnumStringAttribute)
                    {
                        EnumStringAttribute esr = (EnumStringAttribute)at;
                        object obj = Enum.Parse(enumType, mi.Name);
                        if (((int)Enum.Parse(enumType, mi.Name)) == enumVal)
                        {
                            ret = esr.GetEnumString();
                            break;
                        }
                    }
                }
                if (ret != "")
                {
                    break;
                }
            }
            enumStringHash[key] = ret;
            return ret;
        }

        public static List<string> GetEnumString(System.Type type)
        {
            if (typeHash.ContainsKey(type.ToString()))
            {
                return typeHash[type.ToString()];
            }
            List<string> list = new List<string>();
            MemberInfo[] mis = type.GetMembers();
            foreach (MemberInfo mi in mis)
            {
                Attribute[] attrs = Attribute.GetCustomAttributes(mi);
                foreach (Attribute at in attrs)
                {
                    if (at is EnumStringAttribute)
                    {
                        list.Add(((EnumStringAttribute)at).GetEnumString());
                    }
                }
            }
            typeHash[type.ToString()] = list;
            return list;
        }
    }

    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = false)]
    public class EnumStringAttribute : Attribute
    {
        private string enumStr;

        public EnumStringAttribute(string str)
        {
            this.enumStr = str;
        }

        public string GetEnumString()
        {
            return this.enumStr;
        }
    }
}
