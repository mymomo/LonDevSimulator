﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO
{
    public class SensorSemiAuto: CardBase
    {
        public SensorSemiAuto(int cardNo)
            : base(cardNo)
        {
            base.channels = new ChannelBase[8];
            for (int i = 0; i < 8; i++)
            {
                base.Channels[i] = new ChannelSemiAuto(this);
            }
        
        }

    }
}
