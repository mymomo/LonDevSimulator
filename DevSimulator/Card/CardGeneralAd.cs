﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO
{
    public class CardGeneralAd: CardBase
    {
        public CardGeneralAd(int cardNo)
            : base(cardNo)
        {
            
            base.channels = new ChannelBase[96];
            for (int i = 0; i < base.channels.Length; i++)
            {
                base.Channels[i]=new ChannelNull(this);
            }
        }
    }
}
