﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO
{
    public class SensorFskSend: CardBase
    {
        public SensorFskSend(int cardNo)
            : base(cardNo)
        {
            base.channels = new ChannelBase[8];
            for (int i = 0; i < 8; i++)
            {
                base.Channels[i] = new ChannelFSK(this);
            }
            SetVoltDefault();
            SetCurrentDefault();
        }

        private void SetVoltDefault()
        {
            for (int i = 0; i < 4; i++)
            {
                base.Channels[i].Values[0].MaxVal = 30000;
                base.Channels[i].Values[0].Val = 1700;
            }
        }

        private void SetCurrentDefault()
        {
            for (int i = 4; i < 8; i++)
            {
                base.Channels[i].Values[0].MaxVal = 30000;
                base.Channels[i].Values[0].Val = 170;
            }
        }
    }
}
