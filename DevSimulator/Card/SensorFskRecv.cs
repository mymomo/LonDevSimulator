﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO
{
    public class SensorFskRecv: CardBase
    {
        public SensorFskRecv(int cardNo)
            : base(cardNo)
        {
            base.channels = new ChannelBase[4];
            for (int i = 0; i < 4; i++)
            {
                base.Channels[i] = new ChannelFSK(this);
            } 
            SetVoltDefault();
        }
        private void SetVoltDefault()
        {
            
                base.Channels[0].Values[0].MaxVal = 30000;
                base.Channels[0].Values[0].Val = 10000;
                base.Channels[1].Values[0].MaxVal = 7000;
                base.Channels[1].Values[0].Val = 5000;
                base.Channels[2].Values[0].MaxVal = 3000;
                base.Channels[2].Values[0].Val = 2000;
                base.Channels[3].Values[0].MaxVal = 3000;
                base.Channels[3].Values[0].Val = 200;    
                base.Channels[3].Values[1].Val = 23000;
            
        }

    }
}
