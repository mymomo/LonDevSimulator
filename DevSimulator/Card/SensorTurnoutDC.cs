﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO
{
    public class SensorTurnoutDC: CardBase
    {
        public SensorTurnoutDC(int cardNo)
            : base(cardNo)
        {
            base.channels = new ChannelBase[16];
            for (int i = 0; i < 12; i++)
            {
                base.Channels[i]=new ChannelDigit(this);
            }
            for (int i = 12; i < 16; i++)
            {
                base.Channels[i] = new ChannelNull(this);
            }
        }
    }
}
