﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO
{
    public class CardDigit: CardBase
    {
        public CardDigit(int cardNo)
            : base(cardNo)
        {
            base.channels = new ChannelBase[48];
            for (int i = 0; i < 48; i++)
            {
                base.Channels[i] = new ChannelNull(this);
            }
        }
    }
}
