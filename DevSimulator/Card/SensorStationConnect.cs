﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO
{
    /// <summary>
    /// 站间联系电压
    /// </summary>
    public class SensorStaitonConnect: CardBase
    {
        public SensorStaitonConnect(int cardNo)
            : base(cardNo)
        {
            base.channels = new ChannelBase[6];
            for (int i = 0; i < base.channels.Length; i++)
            {
                base.Channels[i] = new ChannelDC(this);
                base.Channels[i].Values[0].MaxVal=1500;
                base.Channels[i].Values[0].MinVal = -1500;
                base.Channels[i].Values[0].Val = 1000;
            }
        
        }

    }
}
