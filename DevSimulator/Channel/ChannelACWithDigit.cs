﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO
{
    public class ChannelACWithDigit:ChannelBase
    {

        public ChannelACWithDigit(CardBase card)
            :base(card)
        {
            this.card = card;
            this.analogValues = new AnalogValue[1];
            AnalogValue ampli = new AnalogValue();
            this.digitValues = new DigitValue[1];
            this.digitValues[0] = new DigitValue();
            this.analogValues[0] = ampli;
            ampli.MaxVal = 800;
            ampli.MinVal = 0;
            
        }


    }
}
