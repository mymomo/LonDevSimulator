﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO
{
    public class ChannelTurnoutState:ChannelBase
    {


        public ChannelTurnoutState(CardBase card)
            : base(card)
        {
            this.card = card;
            this.analogValues = new AnalogValue[2];
            InitAcVal();
            InitDcVal();
        }

        private void InitAcVal()
        {
            AnalogValue ampli = new AnalogValue();
            this.analogValues[0] = ampli;
            ampli.MaxVal = 2000;
            ampli.MinVal = 0;
            ampli.Val = 600;
        }


        private void InitDcVal()
        {
            AnalogValue ampli = new AnalogValue();
            this.analogValues[1] = ampli;
            ampli.MaxVal = 2000;
            ampli.MinVal = 0;
            ampli.Val = 700;
        }


    }
}
