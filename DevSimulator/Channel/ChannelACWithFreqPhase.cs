﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO
{
    public class ChannelACWithFreqPhase:ChannelBase
    {
    

        public ChannelACWithFreqPhase(CardBase card):base(card)
        {
            this.card = card;
            this.analogValues = new AnalogValue[3];
            InitAmpliVal();
            InitFreqVal();
            InitPhaseValue();
        }

        private void InitAmpliVal()
        {
            AnalogValue ampli = new AnalogValue();
            this.analogValues[0] = ampli;
            ampli.MaxVal = 800;
            ampli.MinVal = 0;
            ampli.Val = 220*800/300;
        }

        private void InitPhaseValue()
        {
            AnalogValue phase = new AnalogValue();
            this.analogValues[1] = phase;
            phase.MaxVal = 3600;
            phase.MinVal = 0;
            phase.Val = 120;
        }

        private void InitFreqVal()
        {
            AnalogValue freq = new AnalogValue();
            this.analogValues[2] = freq;
            freq.MaxVal = 520;
            freq.MinVal = 240;
            freq.Val =500;
        }


    }
}
