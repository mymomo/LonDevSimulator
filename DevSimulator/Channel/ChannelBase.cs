﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO
{
    public class ChannelBase 
    {
        protected CardBase card;
        protected AnalogValue[] analogValues=new AnalogValue[0];
        protected DigitValue[] digitValues = new DigitValue[0];

        public DigitValue[] DigitValues
        {
            get { return digitValues; }
         
        }
        public AnalogValue[] Values
        {
            get { return analogValues; }
        }

        public ChannelBase(CardBase card)
        {
            this.card = card;
        }


    }
}
