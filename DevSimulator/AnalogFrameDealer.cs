﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.Data.Station
{
    class AnalogFrameDealer : FrameDealerBase
    {
       // private SampleDevParam10 param = null;
        int bytesCount = 0;
        private SampleDevParam10 param;
        List<AnalogLogicChannelAttrib> logicChannels = new List<AnalogLogicChannelAttrib>(100);

        public AnalogFrameDealer(SampleDevParam10 param)
        {
            this.param = param;
         
        }
        private void CalcAnalogLogicChannel()
        {
            bytesCount = 0;
            for (int i = 0; i < param.CardItems.Count; i++)
            {
                SampleDevCardParam10 card = param.CardItems[i];
                for (int j = 0; j < card.ChannelItems.Count; j++)
                {
                    List<LogicChannelAttrib> logicChannels = card.ChannelItems[j].Attrib.LogicChannels;
                    for (int k = 0; k < logicChannels.Count; k++)
                    {
                        if (logicChannels[k].AnalogByteCount <= 0) continue;
                        AnalogLogicChannelAttrib channel = new AnalogLogicChannelAttrib();
                        channel.StartIndex = bytesCount;
                        channel.BytesCount = logicChannels[k].AnalogByteCount;
                        channel.PointCount = logicChannels[k].PointCount;
                        bytesCount += channel.BytesCount * channel.PointCount;
                    }
                }
            }
        }
        protected override DevDataFrame BuildDevFrame()
        {
            int i = 0;
            for (i = 0; i < tempCache.Count; i++)
            {
                if (tempCache[i].buf[1] != i) break;
            }
            if (i != tempCache.Count) return null;
            List<byte> byteList = GetByteList(true);
            if (byteList.Count != bytesCount)
            {
                //ToDo:加入重新设置函数
                //输出日志信息
                return null;
            }
            DevDataFrame txFrame = new DevDataFrame();

            txFrame.DataTime = tempCache[0].RecTime;
            txFrame.RecvTime = tempCache[0].RecTime;
            txFrame.SampleDevNo = tempCache[0].CanId >> 3;
            txFrame.DataBuf=new AnalogPackage[logicChannels.Count];
            for (i = 0; i < logicChannels.Count; i++)
            {
                for (int j = 0; j < logicChannels[i].PointCount; i++)
                {
                    int val=0;
                    for (int k = 0; k < logicChannels[i].BytesCount; k++)
                    {
                        val <<= 8;
                        val += byteList[logicChannels[i].StartIndex+k];
                    }
                }
            }
            return txFrame;
        }

    }

    class AnalogLogicChannelAttrib
    {
        public  int StartIndex;
        public int PointCount;
        public  int BytesCount;
    }
}
