﻿namespace Lon.Data.Station
{
    partial class FormSampleDevSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.devSelectGrid = new SourceGrid.Grid();
            this.btnGrid = new SourceGrid.Grid();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.devSelectGrid);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.btnGrid);
            this.splitContainer1.Size = new System.Drawing.Size(292, 273);
            this.splitContainer1.SplitterDistance = 175;
            this.splitContainer1.TabIndex = 0;
            // 
            // devSelectGrid
            // 
            this.devSelectGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.devSelectGrid.Location = new System.Drawing.Point(0, 0);
            this.devSelectGrid.Name = "devSelectGrid";
            this.devSelectGrid.OptimizeMode = SourceGrid.CellOptimizeMode.ForRows;
            this.devSelectGrid.SelectionMode = SourceGrid.GridSelectionMode.Cell;
            this.devSelectGrid.Size = new System.Drawing.Size(290, 173);
            this.devSelectGrid.TabIndex = 0;
            this.devSelectGrid.TabStop = true;
            this.devSelectGrid.ToolTipText = "";
            this.devSelectGrid.Paint += new System.Windows.Forms.PaintEventHandler(this.devSelectGrid_Paint);
            // 
            // btnGrid
            // 
            this.btnGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnGrid.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnGrid.Location = new System.Drawing.Point(0, 0);
            this.btnGrid.Name = "btnGrid";
            this.btnGrid.OptimizeMode = SourceGrid.CellOptimizeMode.ForRows;
            this.btnGrid.SelectionMode = SourceGrid.GridSelectionMode.Cell;
            this.btnGrid.Size = new System.Drawing.Size(290, 92);
            this.btnGrid.TabIndex = 0;
            this.btnGrid.TabStop = true;
            this.btnGrid.ToolTipText = "";
            // 
            // FormSampleDevSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 273);
            this.Controls.Add(this.splitContainer1);
            this.DockAreas = ((WeifenLuo.WinFormsUI.Docking.DockAreas)((WeifenLuo.WinFormsUI.Docking.DockAreas.DockLeft | WeifenLuo.WinFormsUI.Docking.DockAreas.DockRight)));
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "FormSampleDevSelect";
            this.Text = "采集机选择";
            this.Load += new System.EventHandler(this.FormSampleDevSelect_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private SourceGrid.Grid btnGrid;
        public SourceGrid.Grid devSelectGrid;

    }
}