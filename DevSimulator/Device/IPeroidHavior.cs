﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO
{
    public interface IPeriodRun
    {
        void Run(ref DateTime startRunTime);
    }
}
