﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO
{
    class OptCmd
    {
        public Int32 DevNo;
        public Int32 CardNo;
        public Int32 Index;
        public Int16 Val;
        public CmdType CmdType;
        public byte[] DataBuf;
        public OptCmd(int devNo,int cardNum, int index, Int16 val)
        {
            this.DevNo = devNo;
            this.CardNo = cardNum;
            this.Index = index;
            this.Val = val;
        }
    }
    public enum CmdType
    {
        Analog,
        Digit,
        Curve,
    }
}
