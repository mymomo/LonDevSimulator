﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO.Ports
{
    public class CanPort : PortBase<CanFrame>
    {
        private static BusOpt<CanFrame> bus = new BusOpt<CanFrame>();

        protected override BusOpt<CanFrame> Bus
        {
            get { return CanPort.bus; }
        }

        public CanPort()
        {

        }

        public CanPort(String portName)
            : base(portName)
        {

        }

        public CanPort(IFilter<CanFrame> fliter)
            : base(fliter)
        {

        }

        public static void Write(CanFrame val)
        {
            bus.Write(val);
        }
    }
}
