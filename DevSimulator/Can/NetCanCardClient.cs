﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using System.IO;
using System.Diagnostics;

namespace Lon.IO.Ports
{
    public class NetCanCardClient : ICanDev
    {

        Thread runThread;
        Thread txThread;

        IPEndPoint ipe = null;
        Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        Socket clientSocket = null;

        private bool isRuning = true;
        public bool IsRunning
        {
            get
            {
                if(this.clientSocket==null||this.clientSocket.Connected)
                {
                    isRuning = false;
                }
                return isRuning;
            }
        }

        public NetCanCardClient(Socket clientSocket)
        {
            this.clientSocket = clientSocket;
            Start();
        }

        public bool Start()
        {
            runThread = new Thread(new ThreadStart(RxProc));
            runThread.IsBackground = true;
            runThread.Start();
            return true;
        }

        public override CanFrame ReadFrame()
        {
            return null;
        }

        public override void WriteCanFrame(CanFrame cf)
        {
            if (clientSocket == null) return;
            if (this.isRuning == false)
            {
                this.clientSocket.Close();
                return;
            }
            try
            {
                byte[] txBuf = BuildDataBuf(cf);
                byte[] sendBuf = BuildNetBuf(txBuf);
                clientSocket.Send(sendBuf);
            }
            catch (System.Exception ex)
            {
                this.isRuning = false;
            }
        }
        protected void RxProc()
        {

            try
            {
                SocketRecive();
            }
            catch (System.Exception ex)
            {
                this.isRuning = false;
            }



        }

        /// <summary>
        /// socket接收处理
        /// </summary>
        private void SocketRecive()
        {

            while (true)
            {
                byte[] buf = new byte[6000];
                int rxCount = clientSocket.Receive(buf);

                Thread.Sleep(1000);


            }
        }


        private byte[] BuildNetBuf(byte[] txBuf)
        {
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);
            bw.Write((byte)0x10);
            bw.Write((byte)0x02);
            for (int i = 0; i < txBuf.Length; i++)
            {
                if (txBuf[i] == 0x10)
                {
                    bw.Write((byte)0x10);
                    bw.Write((byte)0x10);
                }
                else
                {
                    bw.Write(txBuf[i]);
                }
            }
            bw.Write((byte)0x10);
            bw.Write((byte)0x03);
            return ms.ToArray();
        }

        private byte[] BuildDataBuf(CanFrame txFrame)
        {
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);
            byte portNo = 1;
            byte devNo = (byte)(txFrame.CanId >> 3);
            if (devNo > 20 && devNo < 32)
            {
                portNo = 2;
            }
            bw.Write((byte)0x01);
            bw.Write((byte)portNo);
            bw.Write(DateTime.Now.Ticks);
            bw.Write(txFrame.CanId);
            bw.Write((byte)txFrame.Buf.Length);
            bw.Write(txFrame.Buf);
            return ms.ToArray();
        }



    }
}
