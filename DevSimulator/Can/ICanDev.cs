﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO.Ports
{
    public abstract class ICanDev
    {
        public virtual CanFrame ReadFrame()
        {
            return null;
        }

        public virtual void WriteCanFrame(CanFrame cf)
        {
            return;
        }
       
    }
}
