﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO.Ports
{
    public class CanDirFilter : IFilter<CanFrame>
    {
     
        private CanDataDir dir;
        public CanDirFilter(CanDataDir dir)
        {
            this.dir = dir;
        }
        public bool Comp(CanFrame package)
        {
           return package.Dir == dir;
        }

    }
}
