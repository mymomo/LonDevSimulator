﻿using System;
using System.Collections.Generic;
using System.Text;
using Lon.IO;
using Lon.Util;
using System.IO;

namespace Lon.Data.Station
{
    public class SampleDevHelper
    {
        static Dictionary<int, String> errCodeDict = new Dictionary<int, string>();
        static Dictionary<int, String> cardTypeDict = new Dictionary<int, string>();
        static Dictionary<int, String> devTypeDict = new Dictionary<int, string>();
        static Dictionary<int, String> cardSoftTypeDict = new Dictionary<int, string>();
        static SampleDevHelper()
        {
            String appPath = PathHelper.GetApplicationPath();
            IniDoc cfg = new IniDoc(Path.Combine(appPath,@"ModuleCfg\SampleDev\类型定义.ini"));
            cfg.Load();
            int count = 0;
            String val;
            count = cfg.GetInt("故障代码", "数量", 0);
            for(int i=1;i<count+1;i++)
            {
                val=cfg.GetString("故障代码",i.ToString());
                if(String.IsNullOrEmpty(val))
                {
                    errCodeDict[i]="未知";
                }
                else
                {
                    errCodeDict[i] = val;
                }
                
            }

            count = cfg.GetInt("板卡类型", "数量", 0);
            for(int i=1;i<count+1;i++)
            {
                val=cfg.GetString("板卡类型",i.ToString());
                if(String.IsNullOrEmpty(val))
                {
                    cardTypeDict[i]="未知";
                }
                else
                {
                    cardTypeDict[i] = val;
                }
                
            }


            count = cfg.GetInt("采集机类型", "数量", 0);
            for(int i=1;i<count+1;i++)
            {
                val=cfg.GetString("采集机类型",i.ToString());
                if(String.IsNullOrEmpty(val))
                {
                    devTypeDict[i]="未知";
                }
                else
                {
                    devTypeDict[i] = val;
                }
                
            }

            
            count = cfg.GetInt("软件类型", "数量", 0);
            for(int i=0;i<count+1;i++)
            {
                val=cfg.GetString("软件类型",i.ToString());
                if(String.IsNullOrEmpty(val))
                {
                    cardSoftTypeDict[i]=i.ToString();
                }
                else
                {
                    cardSoftTypeDict[i] = val;
                }
                
            }

            
        }

       public static  String GetErrCodeName(int code)
        {
            if (!errCodeDict.ContainsKey(code)) return code.ToString();
            return errCodeDict[code];
        }

       public static  String GetCardTypeName(int code)
        {
            if (!cardTypeDict.ContainsKey(code)) return code.ToString();
            return cardTypeDict[code];
        }

        public static String GetDevTypeName(int code)
        {
            if (!devTypeDict.ContainsKey(code)) return "未知";
            return devTypeDict[code];
        }

        public static String GetCardSoftTypeName(int code)
        {
            if (!cardSoftTypeDict.ContainsKey(code)) return code.ToString();
            return cardSoftTypeDict[code];
        }


      
    }
}
