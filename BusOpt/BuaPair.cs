﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO.Ports
{
    public class BusPair<T>
    {
        BusOpt<T> txBus = null;
        BusOpt<T> rxBus = null;

        public BusOpt<T> TxBus
        {
            get { return txBus; }
        }    

        public BusOpt<T> RxBus
        {
            get { return rxBus; }
        }
        public BusPair(BusOpt<T> txBus, BusOpt<T> rxBus)
        {
            this.txBus = txBus;
            this.rxBus = rxBus;
        }
    
    
    }
}
