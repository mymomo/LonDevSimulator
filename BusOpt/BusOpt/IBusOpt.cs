﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO.Ports
{

    public interface IBusOpt<T>
    {
        
        bool Write(T val);
        bool AddListener(IListener<T> lisenter);
        bool DelListener(IListener<T> lisenter);
      
    }
}
